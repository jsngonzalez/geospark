//
//  AppDelegate.swift
//  geosparkPrueba2020
//
//  Created by IMAC on 10/03/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import GeoSpark
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        GeoSpark.intialize("b9f6be57f17a7cbd98dfbc0007aa99286f65bf98b2085074920d9f8cd7c23f5f")
        GeoSpark.trackLocationInAppState([GSAppState.AlwaysOn])
        GeoSpark.trackLocationInMotion([GSMotion.All])
        GeoSpark.setLocationAccuracy(10)
        GeoSpark.delegate = self
        GeoSpark.enableLogger(true)
        registerForPushNotifications()

        #if compiler(>=5.1)
           if #available(iOS 13.0, *) {
               window?.overrideUserInterfaceStyle = .light
           }
        #endif
        
        return true
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            guard granted else { return }
            self.getNotificationSettings()
            UNUserNotificationCenter.current().delegate = self
        }
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        GeoSpark.setDeviceToken(deviceToken)
    }


}

extension AppDelegate: GeoSparkDelegate {
    
    func didUpdateLocation(_ location: GSLocation) {
        //do something with location, user
        print(location)
    }
    
    func didFail(_ error: GeoSparkError) {
        print("GeoSparkError: ", error.errorCode, error.errorMessage)
    }
}

extension AppDelegate:UNUserNotificationCenterDelegate{
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        return completionHandler([.alert,.sound,.badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        GeoSpark.notificationOpenedHandler(response)
        completionHandler()
    }
}

