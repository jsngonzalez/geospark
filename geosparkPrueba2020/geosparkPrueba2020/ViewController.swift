//
//  ViewController.swift
//  geosparkPrueba2020
//
//  Created by IMAC on 10/03/20.
//  Copyright © 2020 IMAC. All rights reserved.
//

import UIKit
import GeoSpark

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    

    var tripId = "trip\(Int.random(in: 0 ..< 10))"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if UserDefaultsValue.getDefaultString("UserId").isEmpty{
            
            let userDes = "conductor_jeisson"
            GeoSpark.createUser(userDes, { (user) in
                if user.userId.isEmpty == false{
                    DispatchQueue.main.async {
                        UserDefaultsValue.setDefaultString(user.userId, "UserId")
                        print(user.userId)
                        UserDefaultsValue.setDefaultString(userDes, "appUserName")
                    }
                }
            },onFailure: { (error) in
              // error.errorCode
              // error.errorMessage
                print(error.errorCode)
                print(error.errorMessage)
            })
        }else{
            let UserId = UserDefaultsValue.getDefaultString("UserId")
            let appUserName = UserDefaultsValue.getDefaultString("appUserName")
            
            print("UserId:",UserId)
            print("appUserName:",appUserName)
        }
        

        initLabel()
        
    }
    
    func initLabel(){
         label.text = GeoSpark.isLocationTracking() ? "Esta corriendo...": "No esta corriendo"
    }

    @IBAction func comenzar(_ sender: Any) {
        if GeoSpark.isMotionEnabled() == false {
          GeoSpark.requestMotion()
        }else if GeoSpark.isLocationEnabled() == false{
          GeoSpark.requestLocation()
        }else {
          GeoSpark.startTracking()
          initLabel()
        }
        
    }
    
    @IBAction func detener(_ sender: Any) {
        GeoSpark.stopTracking()
        initLabel()
    }
    
    @IBAction func iniciarRuta(_ sender: Any) {
        
        print("tripID:",tripId)
        
        GeoSpark.startTrip("", "", { (trip) in
            print(trip.msg ?? "")
        }, onFailure: { error in
            
        })
        
        GeoSpark.startTrip(tripId,"", { (trip) in
            DispatchQueue.main.async{
                self.label.text = trip.msg
                UserDefaultsValue.setDefaultString(self.tripId, "tripId")
            }
        }, onFailure: { (error) in
                // error.errorCode
            DispatchQueue.main.async{
                self.label.text = "\(error.errorCode): \(error.errorMessage)"
            }
        })
    }
    
    @IBAction func detenerRuta(_ sender: Any) {
        
        let tripid = UserDefaultsValue.getDefaultString("tripId")
        GeoSpark.endTrip(tripid, { (trip) in
            DispatchQueue.main.async{
                self.label.text = trip.msg
                UserDefaultsValue.removeKey("tripId")
                self.tripId = "trip_\(Int.random(in: 0 ..< 10))"
            }
        }, onFailure: { (error) in
                 // error.errorCode
             DispatchQueue.main.async{
                 self.label.text = "\(error.errorCode): \(error.errorMessage)"
             }
        })
    }
    
    
}

